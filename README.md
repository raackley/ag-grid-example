# ag-grid-example

From tutorial [here](https://www.ag-grid.com/javascript-data-grid/getting-started/)

## GitLab Pages

This example deploys to GitLab Pages, which is found [here](https://raackley-open-source.gitlab.io/ag-grid-example/master/)
