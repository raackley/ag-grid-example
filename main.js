const columnDefs = [
  { field: "make", sortable: true, filter: true },
  { field: "model", sortable: true, filter: true },
  { field: "price", sortable: true, filter: true }
];

var url = "data.csv";

/* set up async GET request */
var req = new XMLHttpRequest();
req.open("GET", url, true);
req.responseType = "arraybuffer";

req.onload = function (e) {
  var data = new Uint8Array(req.response);
  var workbook = XLSX.read(data, { type: "array", raw: "true" });

  var firstSheetName = workbook.SheetNames[0];
  var worksheet = workbook.Sheets[firstSheetName];

  // we expect the following columns to be present
  var columns = {
    'A': 'make',
    'B': 'model',
    'C': 'price'
  };

  var rowData = [];

  // start at the 2nd row - the first row are the headers
  var rowIndex = 2;

  // iterate over the worksheet pulling out the columns we're expecting
  while (worksheet['A' + rowIndex]) {
    var row = {};
    Object.keys(columns).forEach(function (column) {
      row[columns[column]] = worksheet[column + rowIndex].v;
    });

    rowData.push(row);

    rowIndex++;
  }

  // let the grid know which columns and what data to use
  const gridOptions = {
    columnDefs: columnDefs,
    rowData: rowData
  };

  // setup the grid
  const gridDiv = document.querySelector('#myGrid');
  new agGrid.Grid(gridDiv, gridOptions);
}

req.send();